<?php
/*
* Címkezelő osztály
*/
class AddressBase {
    //ezek az osztály konstansai
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;
    //érvényes cím típusok statikus tömbje
    public static $valid_address_types=[
        //a fenti értékek változását követi
        //de a self:: az önmaga osztálynevére hivatkozik. Jobb így hivatkozni, mint a tényleges osztálynév kiírásával, mert ha az változik, akkor át kell írni. A self-nél nem kell átírni.
        AddressBase::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary',
    ];

    
    //public, protected, private lehetőség erősségi sorrendben
    //címsor 1
    public $street_address_1;
    //vímsor 2
    public $street_address_2;
    //város
    public $city_name;
    //város rész
    public $subdivision_name;
    //irányítószám
    protected $_postal_code;
    //ország
    public $country_name;
    
    //cím azonosítója, ami a tároláshoz kell
    //cím azonosítója
    //érdemes a protected neveket megjelölni, például így: _
    protected $_address_id;
    //címtípus azonosítója
    protected $_address_type_id;
    //létrehozás és módosítás dátuma
    protected $_time_created;
    protected $_time_updated;
    /*Magic functions*/
    //construct, set, get
    //etzeket csak egyszer deklarálhatom
    /**
     * konstruktor
     * akkor fut, amikor egy new classname fut
     */
    public function __construct($data=[]) {//default paraméter megadása
        var_dump('fut a constructor:',$data);
        //Ha nem üres a tömb, akkor megpróbáljuk feltölteni az objektumot a kapott adatokból.
        //foreach függvénnyel bejárom és kiszedem a kulcsokat és az elemeket.
        //HF        
                
        $this->_time_created=time();
        //ellenőrizzük hogy a kapott adattípus feldolgozható-e
        if(!is_array($data)){
            trigger_error('Nem tudunk felépíteni objektumot a(z) '.get_class($name).' osztály segítségével, mert hibás a kapott adattípus('.gettype($data).').');
        }
        if(count($data) > 0){
            foreach ($data as $name => $value) {
                //kivételek a védett tulajdonságoknak, hogy kívülről is lehessen feltölteni, például db-ből
                if(in_array($name, [
                    'time_created','time_updated'
                ])){
                    $name = '_'.$name;
                }
                $this->$name = $value;
            }
        }
    }
    /**
     * Magic __get
     * akkor fut, amikor egy nem létező tulajdonságot próbálun kmeg az objektumból elérni
     * @param type $name
     */
    public function __get($name){
        //nézzük meg, hogy nincs-e védett tulajdonság ugyanebből
        if(!$this->_postal_code){
            $this->_postal_code = $this->_postal_code_search();
        }
        
        $protected_property_name = '_'.$name;//ha van védett, akkor ez lesz a neve
        //ha ez létezik, akkor állítsuk be
        if(property_exists($this,$protected_property_name)){
            return $this->$protected_property_name;
        }
            trigger_error('Nem létező vagy védett tulajdonságot próbálunk elérni __get által('.$name.')');
            return NULL;
        
        var_dump('fut a get:'.$name);
    }
    /**
     * Magic __set
     * akkor fut, amikor egy nem létező tulajdonságot próbáluink megadni az objektumnak.
     * @param type $name
     * @param mixed $value
     */
    public function __set($name, $value) {
        //címtípus meghadhatóság biztosítása 
        if($name =='address_type_id'){
            $this->_setAddressTypeId($value);
            return;
        }
        //irányítószám, ha van, azt engedjük
        if($name=='postal_code'){
            $this->$name = $value;
            return;//a return-t nem szabad kihagyni, mert akkor tovább fut és kiírja a trigger_error-t az eljárás végén
        }               
        
        //hibakezelé snem létező tulajdonságra
        //a trigger_error notice-t ír ki
        trigger_error('Nem létező tuilajdonságot próbálunk beállítani __set által ('.$name.')');
        
        //var_dump('fut a set:'.$name.'|'.$value);
    }
    /**
     * Magic _toString az objektumok közvetlen kiírásához
     * @return string
     */
        public function __toString() {
            return $this->display();
        }        

        /**
     * Eljárás irányítószám keresésére adott város és városzrész ismeretében
     * @todo a több visszatérést le kell kezelni - nem egyértelmű meghatározás üzenettel
     * @return string
     */
    protected function _postal_code_search(){
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $qry = "SELECT *"
                . " FROM telepulesek";
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $qry.= " WHERE telepules_nev LIKE '%$city_name%'"
                ." AND telepules_resz = '$subdivision_name'"
                ." ORDER BY telepules_nev ";
        //echo $qry;
        $result = $mysqli->query($qry) or die($mysqli->error);
        $mysqli->set_charset("utf8");//kódlap illesztése. Ez az egyetlen eset, ahová nem kell kötőjel az utf után.
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            return $row['irsz'];
        } else {//bejárjuk a tömböt
            while ($row = $result->fetch_assoc()) {
                var_dump('<pre>', $row, '</pre>');
            }
        }
        return 'több van';
    }
    /**
     * Cím kereeséshez készült statikus kereső eljárás
     * @param string $city
     * @return mixed (null vagy array)
     */
    public static function search_addressparts_from_db($city=''){
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        $data = ['postalcode','city','subdivision'];
        return $data;
    }


    //HF az első form-hoz
    public function listAddressPower(){
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $qry = "SELECT *"
            ." FROM telepulesek";
        $city_name = $mysqli->real_escape_string($this->city_name);
        $qry .= " WHERE telepules_nev LIKE '%$city_name%'"
                ." ORDER BY irsz";
        $result = $mysqli->query($qry) or die($mysqli->error);
        $mysqli->set_charset("utf8");//kódlap illesztése. Ez az egyetlen eset, ahová nem kell kötőjel az utf után.
        $addressParts='';
        
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            return $row['irsz'];
        } else {//bejárjuk a tömböt
            while ($row = $result->fetch_assoc()) {                
                //echo "<p>".$row['irsz']." - ".$row['telepules_nev']." - ".$row['telepules_resz']."</p>";
                $addressParts .= "<p>".$row['irsz']." - ".$row['telepules_nev']." - ".$row['telepules_resz']."</p>";
                
            }
                return $addressParts;
        }
    }
    /**
     * Címtípus érvényességének vizsgálata
     * @param int $address_type_id
     * @return boolean
     */
    public static function isValidAddressTypeId($address_type_id){
        //nem kell if, mert ennek a visszatérése boolean
            return array_key_exists($address_type_id, self::$valid_address_types);
    }
    /**
     * Címtípus azonosító beállítása ha érvényes
     * @param int $address_type_id
     * @return void
     */
    protected function _setAddressTypeId($address_type_id){
        if(self::isValidAddressTypeId($address_type_id)){
            $this->_address_type_id = $address_type_id;
            return;
        } else {
            trigger_error('Nem sikerült elmenteni a címet. Bocsi :(');
        }
    }

    /**
     * Cím kiírása
     * uses bootstrap classe
     * $return string
     */
    public function display(){
        
        $output = '<div class="panel-body">';
        $output .= '<div>'.$this->street_address_1;
        $output .= '<br/>'.$this->street_address_2.'</div>';
        $output .= '<div>'.$this->postal_code.', '.$this->city_name.'</div>';
        
        if($this->subdivision_name){
            $output .= '<div>'.$this->subdivision_name.'</div>';
        }
        
        $output .= '<div>'.$this->country_name.'</div>';
        $output .= '</div>';         
        
        return $output;
    }
}