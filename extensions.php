<?php

/**
 * Autoloader a classok betöltésére
 * @param type $class_name
 */
function __autoload($class_name){
    include 'class.'.$class_name.'.inc';
}
/*$Address=new Address;
$address = new Address;
echo '<pre>'.var_export($address,true).'</pre>';
$Address=new Address;*/

$data=[
    'street_address_1'=>'Nyár utca 11',
    'street_address_2'=>'1. ajtó',
    'city_name'=>'Dány',
    'country_name'=>'Magyarország',    
    'postal_code'=>2118,    
];
$address_residence = new AddressResidence($data);
//$id=$address_residence->save();
echo '<h2>Sikeres beillesztés, azonosítója: '.$id.'</h2>';

echo '<pre>'.var_export($address_residence,true).'</pre>';
$address_business = new AddressBusiness($data);
echo '<pre>'.var_export($address_business,true).'</pre>';
$address_temporary = new AddressTemporary($data);
echo '<pre>'.var_export($address_temporary,true).'</pre>';
echo $address_residence->display();
echo $address_business->display();
echo $address_temporary->display();

echo '<h1>Cím betöltése adatbázisból cím azonosító (address_id) alapján</h1>';
$address_db= Address::load(5);
$address_db->street_address_1 = "Megváltozott tér 10.";
echo $address_db;

try{//az itt keletkező hibát el kell kapni. Ha nincs hiba, akkor lefut a try. Ha van, akkor jön a catch
$address_db = Address::load(7);
echo $address_db;
$address_db->delete();
} catch(ExceptionAddress $e){
    echo $e;
    //echo '<pre>'.var_export($e,true).'</pre>';
}
$data=[
    'street_address_1'=>'Nyár utca 11',
    'street_address_2'=>'1. ajtó',
    'city_name'=>'Dány',
    'country_name'=>'Magyarország',    
    'postal_code'=>2118,    
];
$address_db_2 = Address::getInstance(0, $data);
