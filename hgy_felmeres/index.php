<?php
require_once 'php/Connection.simple.php';
$tutorialTitle = "Ajax keresés PHP, MySQL , jQuery (és Bootstrap) használatával";
$conn = dbConnect();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $tutorialTitle; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="copyright" content="Ruander - PHP haladó tanfolyam"/>
        <meta name="author" content="Reedyseth"/>
        <meta name="email" content="ibarragan at behstant dot com"/>
        <meta name="description" content="<?php echo $tutorialTitle; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel=stylesheet href="css/style01.css">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <div class="page-header ">
                <h1 class="orangeFont noMargin">Ruander <small> - Horváth György</small></h1>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="blueFont"><?php echo $tutorialTitle; ?></h3>
                    </div>
                </div>
            </div>

            <div class="mainContent">
                <form class="form-horizontal" role="form" method="get">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">Név</label>
                        <div class="input-group col-sm-9">
                            <input id="name" name="name" type="text" class="form-control" placeholder="Type the name" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btnSearch">
                                    <span class="glyphicon glyphicon-search"> Keres</span>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <!-- This table is where the data is display. -->
                    <table id="resultTable" class="table table-striped table-hover">
                        <thead>
                        <th>Azonosító</th>
                        <th>Kereszt Név</th>
                        <th>Vezeték Név</th>
                        <th>Email</th>
                        <th>Mellék</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.10.2.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">

            jQuery(document).ready(function ($) {
                $('.btnSearch').click(function () {
                    makeAjaxRequest();
                });
                /*bill leütésre is menjen*/
                $('#name').on('keyup', function () {
                    console.log($(this).val().length);
                    if($(this).val().length > 2){
                        makeAjaxRequest();
                        return false;
                    }else{
                        $('table#resultTable tbody').html('');
                    }
                });
               
                $('form').submit(function (e) {
                    e.preventDefault();
                    makeAjaxRequest();
                    return false;
                });

                function makeAjaxRequest() {

                    $.ajax({
                        url: 'php/search.php',
                        type: 'get',
                        data: {name: $('input#name').val()},
                        success: function (response) {
                            $('table#resultTable tbody').html(response);
                        }
                    });
                }
            });


        </script>
    </body>
</html>