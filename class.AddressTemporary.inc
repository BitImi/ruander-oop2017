<?php

/* 
 * Address osztály bővítése
 */
class AddressTemporary extends Address {//az extends után mondom meg, hogy melyik osztály bővítse
    //display() eljárás felülírása
    public function display(){
        $output = '<div class="panel panel-primary alert-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Szállítási cím</h3>
                    </div>';
        $output .= parent::display();
        $output .= '</div>';
        return $output;
    }
    
    protected function _init() {
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_TEMPORARY);
    }
}
