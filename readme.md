**PHP oop tanfolyam**

1. alkalom: tudásszint felmérés
2. alkalom: verziókezelés alapok GIT-tel
3. alkalom: elméleti alapok, osztály létrehozása, objektummal kapcsolatos műveletek
4. alkalom: konstruktor befejezése adott feladatra (tömbből objektum felépítés) konstans
    - static, self,:: operátor
    -...
5. alkalom: magic toString alkalmazása, konkrét feladatlista elkészítése, időbecsléssel, .gitignor alkalmazása _dev mappára
    - címadatbázis megtervezése, felépítése
    - adatbázis singleton (egyke) osztály létrehozása
6. alkalom: fejlesztési esettanulmány
    - irányítószám keresés városnév és városnév részlet alapján az elkészített adatbázis egyke (Database singleton) segítségével
    - a felszínre került elvi hibák megoldásainak kidolgozása, a hibák javítása
7. alkalom: statiskus címkeresés elkészítése
    - bootstrap navigáció és url vezérelt tartalom betöltés elkészítése
    - Address osztály bővítése és Address osztály abstratcttá alakítása (init megkövetelése és bővítésekben a konstruktorban veló beállítása)
    - Bővítésből tulajdonság újradeklarálás
    - Bővítséből method override tesztelése, display()
    - Model elkészítése a load és save lejárások megkövetelése
    - Interface alkalmazása az Address osztályon
8. alkalom:
    - AddressBase létrehozása hogy az eredeti Address működés megmaradjon, illetve az alap menüpont működőképes lehessen (abstract és interface előtti állapot)
    - Address::load(id) elkészítése és a példányosítás megvalósítás a dinamikus osztály hívással
    - Address::save() elkészítése. HF: getALL() eljárás interface-ből megkövetelése és megírása az address osztályon
9. alkalom:
    - list tábla HF megoldása, clone védelem
    - Exception kezelése
    - Exception kezelé nem létező cím betöltésekor saját exception kiegészítéssel és hibakóddal. getInstance kiegészítése, ha rossz címtípusazonosÍtóval szeretnénk meghívni, az abstract ősből ne akarjon objektumot felépíteni.
    - törlés megoldása list.php-ban
    
    
    
