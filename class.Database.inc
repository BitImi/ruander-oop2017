<?php

/* 
 * Database class. Csak egyetlen kapcsolatot engedünk (singleton)
 */

class Database {
    //kapcsolat
    private $_connection;
    
    //példány
    private static $_instance;
    /**
     * db példány visszaadása, csak akkor készül új, ha nincs
     * @return db instance
     */
    public static function getInstance(){
        //ez azért kell, mert minden private és nem fogom elérni a csatlakozási adatokat. ezért kell ez az eljárás.
        if(!self::$_instance){
            self::$_instance = new self;
        }
        return self::$_instance;
    }    
    public function __construct() {
        //$this->_connection = new mysqli('localhost', 'godimre', 'c9password', 'gi_oop');
        $this->_connection = new mysqli('localhost', 'root', '', 'gi_oop');
        
        
        //hibakezelés
        if(mysqli_connect_error()){
            trigger_error('Hiba az adatbázis kapcsolat létrehozása során: '.mysqli_connenct_error(), E_USER_ERROR);
        }        
    }
    /**
     * Kapcsolódás visszaadása
     * @return connection
     */
    public function  getConnection(){
        return $this->_connection;
    }
    /**
     * Magic clone üresen klónozás védelemre
     */
    final public function __clone() {}
}