<?php

/* 
 * Saját hibaüzenet osztály az Address-hez
 */

class ExceptionAddress extends Exception {
    public function __toString() {
        //CLASS magic contstans annak az osztálynak a nevét tartalmazza, ami kidobta. PHP_EOL a sorvégződés, konstans
        return __CLASS__.": [$this->code] - $this->message ".PHP_EOL;
    }
}

