<?php
//osztály betöltése. Azért inc, mert az incluide fájlokat nem lehet meghívni böngészőből
include('class.AddressBase.inc');
include('class_Database.inc');
//objektum létrehozuása
$address = new AddressBase;
echo '<h1>Új üres objektum</h1>';
echo '<pre>'.var_export($address, true).'</pre>';
//objektum feltöltése adatopkkal
$address->city_name = "Budapest";
$address->country_name = "Magyarország";
$address->postal_code = "1234";
$address->street_address_1 = 'Kossuth Lajos 100.';
$address->address_type_id = 1;

echo '<h1>Adatok megadása az objektumnak</h1>';
echo '<pre>'.var_export($address, true).'</pre>'; 

//objektum eljárás meghívása
echo '<h1>Cím kiírása objektum eljárással (display())</h1>';
echo '<div class="row">'
. '<div class="col-sm-4">';
echo $address->display();
echo '</div></div>';
//védett tulajdonság tresztelése
//objektum tulajdonság tesztelése
//echo '<h1>Védett tulajdonság tresztelése</h1>';
//$address->address_id= 12345 ;

//egy adat kiírása az objektumból
echo '<h1>Egy adat kiírása az objektumból</h1>';
echo $address->country_name;
//nem létező tulajdonság megadása
echo '<h1>Nem létező adat kiírása az objektumból</h1>';
echo $address->iranyitoszam = "teszt";
echo '<pre>'.var_export($address, true).'</pre>';//nem kerül bele már üres __set esetén sem, amíg nincs set, addig működik, amikor van, akkor már nem

//védett tulajdonság kiírása
echo '<h1>Védett tulajdonság kiírása</h1>';
echo 'Cím azonosító:'.$address->address_id;

echo $address->valami;

//objektum feltöltése tömb segítségével
$data=[
    'street_address_1'=>'ujabb utca 2',
    'city_name'=>'Sárospatak',
    'country_name'=>'Magyarország',
    'postal_code'=>1234,    
];
$address_2=new AddressBase($data);
echo '<h1>Objektum feltöltése tömb segítségével</h1>';
echo '<pre>'.var_export($address_2, true).'</pre>';
//osztály állandó tesztelése - létezik objektum nélkül is
echo '<h1>Osztály állandó tesztelése</h1>';
echo '<ul>'
.'<li>Állandó cím azonosítója: '.AddressBase::ADDRESS_TYPE_RESIDENCE.'</li>'
.'<li>Számlázási cím azonosítója: '.AddressBase::ADDRESS_TYPE_BUSINESS.'</li>'
.'<li>Szállítási vagy ideiglenes cím azonosítója: '.AddressBase::ADDRESS_TYPE_TEMPORARY.'</li>'
.'</ul>';

echo (AddressBase::$valid_address_types);
echo (AddressBase::$valid_address_types[AddressBase::ADDRESS_TYPE_RESIDENCE]);

echo '<h1>Címtípus ellenőrzés ciklus vizsgálattal</h1>';

for ($i=0; $i <= 5; $i++){
    //shorten if-fel
    echo '<br/>'.$i.':'.(AddressBase::isValidAddressTypeId($i) ? '' : ' nem').' érvényes';
}
//objektum kiírása magic method segítségével
echo '<h1>Objektum kiírása magic method segítségével</h1>';
echo $address;
//gyakorlás
//valós vidéki cím feltöltése objektumba
$data=[
    'street_address_1'=>'Nyár utca 11',
    'street_address_2'=>'1. ajtó',
    'city_name'=>'Dány',
    'country_name'=>'Magyarország',    
    'postal_code'=>2118,    
];
$address_3=new AddressBase($data);
echo '<h1>Harmadik cím feltöltése tömb segítségével</h1>';
echo $address_3;
$data=[
    'street_address_1'=>'Nyár utca 11',
    'street_address_2'=>'1. ajtó',
    'city_name'=>'Dány',
    'country_name'=>'Magyarország',    
    //'postal_code'=>2118,    
];
$address_4=new AddressBase($data);
$address_search = new AddressBase($data);
echo '<h1>Negyedik cím feltöltése tömb segítségével, irányítószám kereséssel</h1>';
echo $address_search;

//típuskényszerítés avagy typecasting
$valami = 01234;
var_dump($valami);

echo '<h1 id="hf">Form a cím megadáshoz - HF</h1>';
//form feldolgozása:
//$error = "";
$addressParts = '';
if(!empty($_POST)){
    if((filter_input(INPUT_POST,'mentes'))=='mentes') {
        //var_dump($_POST);
        $error = '<p style="padding:5px" class="bg-success">Feldolgozás alatt.</p>';
        //kiszedem a városnevet és megtisztítom
        if(!empty(filter_input(INPUT_POST,'varosnev')) && strlen(filter_input(INPUT_POST,'varosnev')) >= 3){
            $varosNev = filter_input(INPUT_POST,'varosnev');
            $data = ['city_name'=>$varosNev];
            $address_5 = new AddressBase($data);
            $addressParts=$address_5->listAddressPower();
            //$char = strlen($varosNev);
            /*if($char >= 3){
                $data = ['city_name'=>$varosNev];
                $address_5 = new AddressBase($data);
            } else {
                $varosNev = '';
                $error = '<p style="padding:5px" class="bg-danger">3-nál több karaktert írj be.</p>';
                $address_5 = '';
            }*/
        } else {
            $error = '<p style="padding:5px" class="bg-danger">Töltsd ki a városnevet.</p>';
        }
    } else {
        $error = '<p style="padding:5px" class="bg-danger">Nem került be a submit értéke</p>';
    }
} else {
    $error = '';
    $address_5 = '';    
}
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <form method="post" class="form-horizontal">
            <div class="row">
                <?php echo $error; ?>
                <div class="col-xs-3">
                    <label for="irsz">Irányítószám</label>
                    <input id="irsz" name="irsz" type="text" class="form-control" placeholder="Irányítószám" disabled>
                </div>
                <div class="col-xs-3">
                    <label for="varosnev">Városnév*</label>
                    <input id="varosnev" name="varosnev" type="text" class="form-control" placeholder="Városnév" required>
                </div>
                <div class="col-xs-3">
                    <label for="varosnev">Városrész</label>
                    <input id="varosresz" name="varosresz" type="text" class="form-control" placeholder="Városrész" disabled>
                </div>
                <div class="col-xs-1">
                    <label for="varosnev">Mentés</label>
                    <button type="submit" value="mentes" id="mentes" name="mentes" class="btn btn-primary">Mentés</button>
                </div>
            </div>
        </form>
    </div>
    <?php
    echo "<div class=\"col-md-8 col-md-offset-2\" style=\"margin-top:10px\">";
    echo $addressParts;
    echo '</div>';
    ?>    
</div>
<?php
//statikuis kereső eljárás tesztelése
echo '<h1>Statikuis kereső eljárás tesztelése</h1>';
$addresspart= AddressBase::search_addressparts_from_db();
echo '<pre>'.var_export($addresspart,true).'</pre>';//a var_export ha második paramétere true, akkor string-ként tér vissza.
?>