<?php

/* 
 * Address osztály bővítése
 */
class AddressResidence extends Address {//az extends után mondom meg, hogy melyik osztály bővítse
//  újra deklarálok egy objektum elemet
    //public $country_name = 'USA';
    //display() eljárás felülírása, csak annyi paramétere lehet, mint az eredetié, azaz itzt most egy sem.
    public function display(){
        $output = '<div class="panel panel-primary alert-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Állandó lakcím</h3>
                    </div>';
        $output .= parent::display();
        $output .= '</div>';
        return $output;
    }


    protected function _init() {
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_RESIDENCE);
    }
}
