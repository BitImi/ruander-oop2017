 <?php
/**
 * interface az interakcióknak
 */
interface Model{
    /**
     * "Cím" (model) betöltése
     * @param int $address_id
     */
    public static function load($address_id);
    
    /**
     * "Cím" (model) mentése
     */
    public function save();
    public function delete();
    /**
     * DB-ben szereplő összes bevitt cím kilistázása.
     */
    /**
     * Eljárás az adott osztály elemeinek a lekérdezésére
     * @param int $slice
     * @param int $from
     */
    public static function getAll($slice,$from);
    //Nem értem, hogy hol és hogyan írjam meg, hiába csinálok bármit is a list.php-ban, valami biztosan hibára fut. Ha más nem, akkor a fetch-assoc(); Mindent ugyanúgy csináltam, mint a load()-nál, de nem működik. Hiába töltöm be a megfelelő fájlokat, valamin mindig elhasal.
}