<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <form method="post" class="form-horizontal">
            <div class="row">                
                <div class="col-xs-3">
                    <label for="irsz">Irányítószám</label>
                    <input id="irsz" name="irsz" type="text" class="form-control" placeholder="Irányítószám" disabled>
                </div>
                <div class="col-xs-3">
                    <label for="varosnev">Városnév*</label>
                    <input id="varosnev" name="varosnev" type="text" class="form-control" placeholder="Városnév" required>
                </div>
                <div class="col-xs-3">
                    <label for="varosnev">Városrész</label>
                    <input id="varosresz" name="varosresz" type="text" class="form-control" placeholder="Városrész" disabled>
                </div>
                <div class="col-xs-3">
                    <label for="varosnev">Városrész</label>
                    <select name='adtype'>
                        <option value='1' selected>lakcím</option>
                        <option value='2'>számlázási cím</option>
                        <option value='3'>szállítási cím</option>
                    </select>                    
                </div>
                <div class="col-xs-1">
                    <label for="varosnev">Mentés</label>
                    <button type="submit" value="mentes" id="mentes" name="mentes" class="btn btn-primary">Mentés</button>
                </div>
            </div>
        </form>
    </div>