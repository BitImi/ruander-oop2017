<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
//vizsgáljuk meg, hogy paraméterből mit kapunk, milyen belső file-t szeretnénk includolni
$includeFile = (filter_input(INPUT_GET, 'p') ? filter_input(INPUT_GET, 'p') : 'test').'.php';
?>
<html>
    <head>
        <meta charset="utf8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <title>Ruander OOP - PHP haladó tanfolyam</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body style="margin-top: 60px">
        <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">OOP tanfolyam</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class=""><a href="/">Alap</a></li>
            <li><a href="?p=form">Űrlap</a></li>
            <li><a href="?p=extensions">Bővítések</a></li>
            <li><a href="?p=list">CRUD</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <div class="container">
            <div class="row">
                <header class="col-xs-12">
                    <h1>Ruander OOP - PHP haladó tanfolyam</h1>
                    <p><a href="http://oop-tanfolyam.local">oop-tanfolyam.local</a></p>
                </header>
            </div>
            <div class="row">
                <article class="col-xs-12">                    
                    <?php
                    if(file_exists($includeFile)){
                        include ($includeFile);
                    } else {
                        echo 'Nem tudom a kívánt filet-t betölteni';//dismissable allert
                    }
                    ?>
                </article>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        Minden tartalom védett &copy; <?php echo date('Y'); ?> Ruander
                    </div>
                    <div class="col-md-6 col-xs-12">
                        Készítette Godla Imre
                    </div>
                </div>
            </div>
        </footer>
        <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous">
        </script>
    </body>
</html>