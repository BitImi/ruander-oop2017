<?php
/**
 * Created by PhpStorm.
 * User: Godla Imre
 * Date: 2017. 03. 05.
 * Time: 17:18

 */
/**
 * Classok betöltése
 * @param type $class_name
 */
function __autoload($class_name){
    include 'class.'.$class_name.'.inc';
}

/*$data=[
    'street_address_1'=>'Nyár utca 10',
    'street_address_2'=>'1. ajtó',
    'city_name'=>'Dány',
    'country_name'=>'Magyarország',
    'postal_code'=>2118,
];
$address_residence = new AddressResidence($data);
$id=$address_residence->save();
echo '<h2>Sikeres beillesztés, azonosítója: '.$id.'</h2>';*/

$db = Database::getInstance();
$mysqli = $db->getConnection();
$mysqli->set_charset("utf8");
/*$query = "SELECT * FROM addresses";
$mysqli->query($query) or die($mysqli->error);
$row = $mysqli_fetch_assoc($mysqli);
*/
//var_dump($_POST);
echo '<h1>HF sima PHP megoldással, működik:</h1>';
$slice = 0;
$from = 4;
$q = mysqli_query($mysqli,"SELECT * FROM addresses LIMIT $slice, $from");
while ($row = mysqli_fetch_array($q)){
    $cim = $row['postal_code'].' '.$row['city_name'].', '.$row['street_address_1'];
    echo $cim.'<br/>';
}
echo '<h1>HF OOP megoldással, nem működik:</h1>';
$slice = 0;
$from = 4;
//getAll($slice,$from);

if(filter_input(INPUT_GET, 'del')){//ha törölni kell, akkor törlüónk
    try{
        $address_delete= Address::load(filter_input(INPUT_GET, 'del'));
        $address_delete->delete();
    } catch (ExceptionAddress $e) {
        echo $e;
    }
    
}

$addresses = Address::getAll();

?>
<div class="container">
    <div class="row">


        <div class="col-md-12">
            <h4>Bootstrap Snipp for Datatable</h4>
            <?php
            if(filter_input(INPUT_POST, 'formuj')){
                //var_dump($_POST);die();
                $cityName = filter_input(INPUT_POST,'varosnev');
                $postalCode = filter_input(INPUT_POST,'irsz');
                $streetAddress1 = filter_input(INPUT_POST,'varosresz1');
                $streetAddress2 = filter_input(INPUT_POST,'varosresz2');
                $countryName = filter_input(INPUT_POST,'orszag');
                $addressTypeId = (int)filter_input(INPUT_POST,'adtype');                
                $data = [
                'street_address_1' => $streetAddress1,
                'street_address_2' => $streetAddress2,
                'city_name' => $cityName,
                'country_name' => $countryName,
                'postal_code' => $postalCode,
                ];
                //var_dump($data); die();
                $obi = Address::getInstance($addressTypeId, $data);
                //var_dump($obi); die();
                if($obi->save()){
                    header("location: ?p=list");
                }
            }
            if(filter_input(INPUT_POST, 'formszerk')){
                //var_dump($_POST);die();
                $cityName = filter_input(INPUT_POST,'varosnev');
                $postalCode = filter_input(INPUT_POST,'irsz');
                $streetAddress1 = filter_input(INPUT_POST,'varosresz1');
                $streetAddress2 = filter_input(INPUT_POST,'varosresz2');
                $countryName = filter_input(INPUT_POST,'orszag');
                $addressTypeId = (int)filter_input(INPUT_POST,'adtype');
                $addressId = (int)filter_input(INPUT_POST,'id');
                $data = [
                'street_address_1' => $streetAddress1,
                'street_address_2' => $streetAddress2,
                'city_name' => $cityName,
                'country_name' => $countryName,
                'postal_code' => $postalCode,
                'address_type_id' => $addressTypeId,
                'address_id' => $addressId
                ];
                //var_dump($data); die();
                try{
                                    
                $obi = Address::getInstance($addressTypeId, $data);
                } catch (Exception $ex) {
                    echo $ex;
                }
                //var_dump($obi); die();
                if($obi->save()){
                    //header("location: http://oop-tanfolyam.local/?p=list");                    
                }
                
            }
            ?>
            <div class="table-responsive">
                <a class="col-md-12 btn btn-success" data-title="Uj" data-toggle="modal" data-target="#ujcim">Új cím</a>
                <!-- Modal -->
                <div class="modal fade" id="ujcim" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Új cím felvitele</h4>
                      </div>
                      <div class="modal-body">                        
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form method="post" class="form-horizontal" id="formuj">
                                    <div class="row">                
                                        <div class="col-xs-12">
                                            <label for="irsz">Irányítószám</label>
                                            <input id="irsz" name="irsz" type="text" class="form-control" placeholder="" readonly>
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="varosnev">Városnév*</label>
                                            <input id="varosnev" name="varosnev" type="text" class="form-control" >
                                        </div>
                                        <div id="searchresults" class="col-xs-12">
                                            Valami
                                        </div>
                                        <div class="col-xs-12">
                                           <label for="varosresz1">Városrész</label>
                                           <input id="varosresz1" name="varosresz1" type="text" class="form-control" readonly>
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="varosresz2">Városrész</label>
                                            <input id="varosresz2" name="varosresz2" type="text" class="form-control" >
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="orszag">Ország</label>
                                            <input id="orszag" name="orszag" type="text" class="form-control" >
                                        </div>                                        
                                        <div class="col-xs-12">
                                            <label for="adtype">Cím típus</label>
                                            <select name="adtype">
                                                <option value="1" selected>lakcím</option>
                                                <option value="2">számlázási cím</option>
                                                <option value="3">szállítási cím</option>
                                            </select>                    
                                        </div>                                        
                                    </div>
                                </form>
                                
                            </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Mégsem</button>
                        <button type="submit" form="formuj" value="mentes" id="mentes" name="formuj" class="btn btn-primary">Mentés</button>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                
                
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <th><input type="checkbox" id="checkall" /></th>
                    <th>id</th>
                    <th>Név</th>
                    <th>Cím</th>                    
                    <th>Típus</th>
                    <th>Szerkeszt</th>
                    <th>Töröl</th>
                    </thead>
                    <tbody>
        <?php
        foreach ($addresses as $k => $address){
            echo '<tr>
                <td><input type="checkbox" class="checkthis" /></td>
                <td>'.$address['address_id'].'</td>
                <td>Teszt név</td>
                <td>'.$address['country_name'].' | '.$address['postal_code'].' | '.$address['city_name'].', '.$address['street_address_1'].', '.$address['street_address_2'].'</td>
                <td>'.Address::$valid_address_types[$address['address_type_id']].'</td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#szerk'.$address['address_id'].'" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Delete"><a href="?p=list&del='.$address['address_id'].'" class="btn btn-danger btn-xs" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></a></p></td>
                </tr>                
            
                <!-- Modal -->
                <div class="modal fade" id="szerk'.$address['address_id'].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cím szerkesztése</h4>
                      </div>
                      <div class="modal-body">
                        <p>'.$address['address_id'].'</p>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form method="post" class="form-horizontal" id="formszerk'.$address['address_id'].'">
                                    <div class="row">
                                        <input style="display:none" id="id" name="id" type="text" class="form-control" value='.$address['address_id'].'>
                                        <div class="col-xs-12">
                                            <label for="irsz">Irányítószám</label>
                                            <input id="irsz" name="irsz" type="text" class="form-control" value='.$address['postal_code'].'>
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="varosnev">Városnév*</label>
                                            <input id="varosnev" name="varosnev" type="text" class="form-control" value='.$address['city_name'].'>
                                        </div>
                                        <div class="col-xs-12">
                                           <label for="varosresz1">Városrész</label>
                                            <input id="varosresz1" name="varosresz1" type="text" class="form-control" value="'.$address['street_address_1'].'">
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="varosresz2">Városrész</label>
                                            <input id="varosresz2" name="varosresz2" type="text" class="form-control" value="'.$address['street_address_2'].'">
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="orszag">Ország</label>
                                            <input id="orszag" name="orszag" type="text" class="form-control" value="'.$address['country_name'].'">
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="adtype">Cím típus</label>
                                            <select name="adtype">
                                                <option value="1" selected>lakcím</option>
                                                <option value="2">számlázási cím</option>
                                                <option value="3">szállítási cím</option>
                                            </select>                    
                                        </div>                                            
                                    </div>
                                </form>
                            </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Mégsem</button>
                        <button type="submit" form="formszerk'.$address['address_id'].'" value="mentes" id="mentes" name="formszerk" class="btn btn-primary">Mentés</button>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
';
}
        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- js a kereséshez -->
<script type="text/javascript">
            jQuery(document).ready(function ($) {                
                /*bill leütésre is menjen*/
                $('#varosnev').on('keyup', function () {
                    console.log($(this).val().length);                    
                    if($(this).val().length > 2){
                        
                        makeAjaxRequest();
                        return false;
                    }else{
                        $('#searchresults').css("display", "none");
                        $('#searchresults').html('');
                    }
                });                               
                function makeAjaxRequest() {

                    $.ajax({
                        url: 'ajaxsearch.php',
                        type: 'get',
                        data: {varosnev: $('input#varosnev').val()},
                        success: function (response) {
                            $('#searchresults').css("display", "block");
                            $('#searchresults').html(response);
                            
                            $("#searchresults li").click(function(){                
                                var varosli = $(this).find("span").first().text();
                                console.log(varosli);
                               $('[id$=varosnev]').val(varosli);
                                
                                var irszli = $(this).find("span:nth-of-type(2)").text();
                                console.log(irszli);
                                $('[id$=irsz]').val(irszli);
                                
                                var varosreszli = $(this).find("span:nth-of-type(3)").text();
                                console.log(varosreszli);
                                $('[id$=varosresz1]').val(varosreszli);
                                //végül a lista visszazárása
                                $("#searchresults").slideUp();
                                });
                               
                        }
                    });
                }
            });
</script>
<!-- js input kitöltéshez -->
<script>
            //listaelem katintás esemény
            /*$(".test-list li").click(function(){
                var li = $(this);
                //betöltjük a kívánt value értékébe az adott elem id-ját.
                $("#testid").val(li.data("id"));                
                //console.log(this);
                $("#test-list-id").slideUp();//css("display","none");
            });
            console.log('valami');*/                
</script>